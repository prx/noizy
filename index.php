<?php
// RSS2socials : link RSS feeds to social networks
// thuban <thuban@yeuxdelibad.net>
// LICENSE : MIT
//
// Requires : php-curl
// This project uses : 
// https://framagit.org/MaxKoder/TootoPHP

define('_NOIZYEXEC', true); 
require_once('boot.php');
require_once('functions.php');

// Configuration unreadable
if (!import_ini_file(CONFIG_FILE)) { 
    header('Location: install.php'); 
}
else if (!defined('RSS_SOURCE')) {
    header('Location: install.php'); 
}
else if (!defined('INSTALL_DONE') || (INSTALL_DONE != 1) ) {
    header('Location: install.php'); 
}
// Config is loaded, go on
else {
    dohead();
    // AJAX update
    echo '<pre><code>';
    echo '<div id="updateoutput">Check new feeds...</div>';
    echo '<script type="text/javascript" src="js/update.js"></script>';
    echo '<noscript>You need javascript enabled, or go directly to <a href="update.php">update page</a>';
    echo '</code></pre>';
    dofooter();
}

?> 
