To add a new engine : 

- Create a directory in engines/
- Edit install.php to add the configuration for your engine.
- Edit functions.php to add a function called "publish_theengine"
- In functions.php, edit "publish()" function to call "publish_theengine" if needed.