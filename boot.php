<?php
defined( '_NOIZYEXEC' ) or die( 'Access Denied!' );

// consts
define('APPNAME', 'Noizy');
define('APPVER', '0.4');
define('APPURL', 'https://framagit.org/Thuban/noizy');
define('DATA_DIR', './'.'data');
define('LASTUPDATE_FILE', DATA_DIR.'/lastupdate');
define('CONFIG_FILE', DATA_DIR.'/config.ini');
define('SLEEPTIME', 2);

//    ---
// GZIP
if (!extension_loaded('zlib')) {
    if (ob_get_length() > 0) {
        ob_end_clean();
    }
    ob_start('ob_gzhandler');
}
// Use UTF-8 for all
mb_internal_encoding('UTF-8');

?>
