<?php
define('_NOIZYEXEC', true); 
require_once('boot.php');
require_once('functions.php');

/* Show installation form
 * If you want to add a new engine, hack here
 */
function install_form()
{
    //try to get already set options
    import_ini_file(CONFIG_FILE);
    echo '
    <h2>CONFIGURATION</h2>
    <h3>General</h3>
    <form class="formstyle" method="post" action="install.php" >
        <ul>
        <li><label>'.'RSS feed to publish'.' </label>
            <input type="url" name="rss_source" size="40" ';

            if (defined('RSS_SOURCE')) {
                echo 'value="'.RSS_SOURCE.'"';
            } else {
                echo 'value=""';
            }
            echo '
            placeholder="https://yeuxdelibad.net/Blog/rss.php?mode=links" required />
            </li>
        <li><label>'.'URL shorten'.'</label>

        <input type="checkbox" name="use_huitre" ';
            if (defined('USE_HUITRE')) { echo 'checked'; }
        echo '>';
        
        echo 'Use <a href="https://huit.re">huit.re</a> to shorten URL
        </li>
        
        </ul>

        <button type="submit" name="enregistrer">Save</button>
        <input type="hidden" name="i" value="installing" />
        </form>';

    echo '
        <h3 onclick="toggle_visibility(\'mastodon_config\');">Mastodon account</h3>
        <div id="mastodon_config">
            <form class="formstyle" method="post" action="install.php" >
                <ul>
                <li>
                    <label>'.'Mastodon instance : '.' </label> 
                    <input type="text" id="mastodon_instance" name="mastodon_instance" 
                        oninput="enable_token_btn(this.value, \'mastodon_token_btn\');" 
                        value="" 
                        pattern="[A-Za-z0-9]*\.[a-z]{2,3}"
                        placeholder="framapiaf.org" 
                        />
                </li>
                <li>
                    <label>'.'Copy mastodon token here : '.' </label>
                    <input type="text" name="mastodon_token" value="" placeholder="" />
                    <input type="button" id="mastodon_token_btn" value="Get mastodon token" disabled 
                        onclick="getInstance(\'mastodon_instance\', \'mastodon\');" />
                </li>
                <li>
                    <label>'.'Mastodon email'.' </label>
                    <input type="email" name="mastodon_email" value="" placeholder="yoshi@framapiaf.org" />
                </li>
                <li>
                    <label>'.'Mastodon password'.' </label>
                    <input type="password" name="mastodon_pw" value="" />
                </li>
                </ul>
            <button type="submit" name="enregistrer">Save</button>
            <input type="hidden" name="i" value="installing" />
            <input type="hidden" name="mastodon" value="configured" />
            </form>
        </div>';

    echo '
        <h3 onclick="toggle_visibility(\'twitter_config\');">Twitter account</h3>
        <div id="twitter_config">
            <form class="formstyle" method="post" action="install.php" >
                <ul>
                <li>
                    <label>'.'Consumer Key (API key) : '.' </label> 
                    <input type="text" name="twitter_api_key" />
                    <br />
                    <a href="https://apps.twitter.com/app/" target="_blank" title="Create twitter app">Create twitter application</a>
                </li>
                <li>
                    <label>'.'Consumer Secret (API secret) : '.' </label> 
                    <input type="text" name="twitter_api_secret" />
                </li>
                <li>
                    <label>'.'Access Token : '.' </label> 
                    <input type="text" name="twitter_access_token" />
                </li>
                <li>
                    <label>'.'Access Token Secret : '.' </label> 
                    <input type="text" name="twitter_access_token_secret" />
                </li>
                </ul>
            <button type="submit" name="enregistrer">Save</button>
            <input type="hidden" name="i" value="installing" />
            <input type="hidden" name="twitter" value="configured" />
            </form>
        </div>';


    echo '
        <form class="formstyle" method="post" action="install.php" >
            <input type="checkbox" name="configdone" value="configdone"> I have finished configuration 
            <button type="submit" name="enregistrer">Save</button>
            <input type="hidden" name="i" value="installing" />
        </form>
        <br />
        <a href="install.php?reset" title="Reset configuration">Reset configuration</a>
    <script type="text/javascript" src="js/install.js"></script>';
}

/* Create directories
 * Show install form
 * Record configuration
 */
function install() {
    // Prepare structure
    if (!file_exists(DATA_DIR)) {
        if (!mkdir(DATA_DIR, 0777, true)) {
            die('Failed to create folders... check permissions');
        }
    }

    if (isset($_POST['i'])) {
        $data = '';

        if (!empty($_POST['rss_source']) && !defined('RSS_SOURCE')) {
            $rss_source = (string)filter_input(INPUT_POST, 'rss_source', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $rss_source = 'RSS_SOURCE = \''.$rss_source.'\''."\n\n\n";
            file_put_contents(CONFIG_FILE, $rss_source, LOCK_EX | FILE_APPEND);
        }
        if (!empty($_POST['use_huitre'])) {
            file_put_contents(CONFIG_FILE, "USE_HUITRE = 1\n", LOCK_EX | FILE_APPEND);
        }
        // Get possible mastodon info
        if (!empty($_POST['mastodon'])) {
            $mastodon_instance = (string)filter_input(INPUT_POST, 'mastodon_instance', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $mastodon_token = (string)filter_input(INPUT_POST, 'mastodon_token', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $mastodon_email = (string)filter_input(INPUT_POST, 'mastodon_email', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $mastodon_pw = (string)filter_input(INPUT_POST, 'mastodon_pw', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            // all must be defined
            if (strlen($mastodon_instance.$mastodon_token.$mastodon_email.$mastodon_pw) > 0 ) {
                register_mastodon_token($mastodon_instance, $mastodon_token, $mastodon_email, $mastodon_pw);
                $data .= 'MASTODON_INSTANCE = \''.$mastodon_instance.'\''."\n";
            }
        }
        // Get possible twitter info
        if (!empty($_POST['twitter'])) {
            $twitter_api_key = (string)filter_input(INPUT_POST, 'twitter_api_key', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $twitter_api_secret = (string)filter_input(INPUT_POST, 'twitter_api_secret', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $twitter_access_token = (string)filter_input(INPUT_POST, 'twitter_access_token', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $twitter_access_token_secret = (string)filter_input(INPUT_POST, 'twitter_access_token_secret', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            // all must be defined
            if (strlen($twitter_api_key.$twitter_api_secret.$twitter_access_token.$twitter_access_token_secret) > 0 ) {
                $data .= 'TWITTER_API_KEY = \''.$twitter_api_key.'\''."\n";
                $data .= 'TWITTER_API_SECRET = \''.$twitter_api_secret.'\''."\n";
                $data .= 'TWITTER_ACCESS_TOKEN = \''.$twitter_access_token.'\''."\n";
                $data .= 'TWITTER_ACCESS_TOKEN_SECRET = \''.$twitter_access_token_secret.'\''."\n";
            }
        }

        // write config if necessary
        if (strlen($data) > 0) {
            file_put_contents(CONFIG_FILE, $data, LOCK_EX | FILE_APPEND);
        }

        // Did we check "configdone" ?
        if (!empty($_POST['configdone'])) { 
            $done = "INSTALL_DONE = 1\n";
            file_put_contents(CONFIG_FILE, $done, LOCK_EX | FILE_APPEND);
        }

        // finally, reload page to eventually complete install
        header('Location: index.php');

    }

    // Register application
    else if (isset($_GET['registerApp'])) {
        $engine = (string)filter_input(INPUT_GET, 'registerApp', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $instance = (string)filter_input(INPUT_GET, 'instance', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ($engine == 'mastodon') {
            get_mastodon_token($instance);
        }

    }
    else if (isset($_GET['reset'])) {
        unlink(CONFIG_FILE);
        header('Location: index.php');
    }
    else {
        install_form();
    }
}


dohead();
install();
dofooter();

?> 
