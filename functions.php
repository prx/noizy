<?php
defined( '_NOIZYEXEC' ) or die( 'Access Denied!' );

require_once('boot.php');

function dohead() {
    echo '<!doctype html>
<html>
<head>
    <title>'.APPNAME.'</title>
    <link rel="icon" href="/favicon96.png" type="image/png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <h1>'.APPNAME.'</h1>
    <h2 id="subtitle">Fête du bruit</h2>
    <p>
        <img alt="noizy logo" id="logo" src="logo.svg" />
    </p>
    <main>';
}

function dofooter() {

    echo '
    </main>
    <footer>
        <a href="'.APPURL.'">'.APPNAME.' - '.APPVER.'</a> - 
        <a href="https://yeuxdelibad.net/Divers/Contact.html#encouragements">♥ donate</a> 
    </footer>
</body>
</html>';
}


/**
 * Import several .ini config files with this function
 * and make ini var as a php constant
 *
 * @param string $file_path, the ini absolute path
 * @return bool
 */
function import_ini_file($file_path)
{
    if (is_file($file_path) and is_readable($file_path)) {
        $options = parse_ini_file($file_path);
        foreach ($options as $option => $value) {
            if (!defined($option)) {
                define($option, $value);
            }
        }
        return true;
    }
    return false;
}

function limit_size($maxchar,$status){
    while(strlen($status) > $maxchar) {
        // remove last word
        // https://codereview.stackexchange.com/questions/8683/remove-last-word-from-string
        $lastSpacePosition = strrpos($status, ' ');
        $status = substr($status, 0, $lastSpacePosition);
    }
    return $status;
}

// MASTODON
function get_mastodon_token($instance) 
{
    $app = register_mastodon($instance);
    echo 'Redirecting to '.$app->getAuthUrl();
    header('Location: '.$app->getAuthUrl());
}

function register_mastodon($instance) {
    require_once 'engines/tootophp/autoload.php';

    $tootoPHP = new TootoPHP\TootoPHP($instance);

    $app = $tootoPHP->registerApp(APPNAME, APPURL);
    if ( $app === false) {
        throw new Exception('Problem during register app');
    }
    return $app;
}

function register_mastodon_token($instance, $accessToken, $email, $pw) 
{
    require_once 'engines/tootophp/autoload.php';
    $app = register_mastodon($instance);
    $token = $app->registerAccessToken($accessToken);
    $bearer_token = $app->authentify($email, $pw);
}

function publish_mastodon($status)
{
    require_once 'engines/tootophp/autoload.php';
    $app=register_mastodon("framapiaf.org");
    $max = 500;
    $status = limit_size($max, $status);
    $app->postStatus($status);
}


// TWITTER
/* Parts stolen here : 
https://github.com/ArthurHoaro/shaarli2twitter/blob/master/shaarli2twitter/shaarli2twitter.php
*/
/**
 * Use TwitterAPIExchange to publish the tweet.
 *
 * @param ConfigManager $conf
 * @param string        $tweet
 *
 * @return string JSON response string.
 */
function publish_tweeter($tweet)
{

    $max = 140;
    $tweet = limit_size($max, $tweet);

    require_once 'engines/twitter-api/TwitterAPIExchange.php';
    $endpoint = 'https://api.twitter.com/1.1/statuses/update.json';
    $postfields = array(
        'status' => $tweet
    );
    $settings = array(
        'consumer_key' => TWITTER_API_KEY,
        'consumer_secret' => TWITTER_API_SECRET,
        'oauth_access_token' => TWITTER_ACCESS_TOKEN,
        'oauth_access_token_secret' => TWITTER_ACCESS_TOKEN_SECRET,
    );
    $twitter = new TwitterAPIExchange($settings);
    return $twitter->buildOauth($endpoint, 'POST')
                   ->setPostfields($postfields)
                   ->performRequest();
}

/*
this function publish status to every configured
social site
*/
function publish($status)
{
    if (defined('MASTODON_INSTANCE')) {
        echo "   ... to mastodon\n";
        publish_mastodon($status);
    }
    if (defined('TWITTER_API_KEY') &&
        defined('TWITTER_API_SECRET') &&
        defined('TWITTER_ACCESS_TOKEN') &&
        defined('TWITTER_ACCESS_TOKEN_SECRET') ) { 
        echo "   ... to twitter\n";
        publish_tweeter($status);
        }
}

// https://stackoverflow.com/questions/2912262/convert-rss-pubdate-to-a-timestamp
function rsstotime($rss_time) {
    $day = substr($rss_time, 5, 2);
    $month = substr($rss_time, 8, 3);
    $month = date('m', strtotime("$month 1 2011"));
    $year = substr($rss_time, 12, 4);
    $hour = substr($rss_time, 17, 2);
    $min = substr($rss_time, 20, 2);
    $second = substr($rss_time, 23, 2);
    $timezone = substr($rss_time, 26);

    $timestamp = mktime($hour, $min, $second, $month, $day, $year);

    date_default_timezone_set('UTC');

    if(is_numeric($timezone)) {
        $hours_mod = $mins_mod = 0;
        $modifier = substr($timezone, 0, 1);
        $hours_mod = (int) substr($timezone, 1, 2);
        $mins_mod = (int) substr($timezone, 3, 2);
        $hour_label = $hours_mod>1 ? 'hours' : 'hour';
        $strtotimearg = $modifier.$hours_mod.' '.$hour_label;
        if($mins_mod) {
            $mins_label = $mins_mod>1 ? 'minutes' : 'minute';
            $strtotimearg .= ' '.$mins_mod.' '.$mins_label;
        }
        $timestamp = strtotime($strtotimearg, $timestamp);
    }

    return $timestamp;
}


// check rss and publish
function check_n_publish($rss_source)
{
    // get last update time.
    $lastupdate = (int)file_get_contents(LASTUPDATE_FILE);
    if (!$lastupdate) {
        // first run, we set to now
        $lastupdate = strtotime("now");
    }
    $maxpubdate = $lastupdate;

    echo "Last item seen on ". date("d/m/Y - H:i\r\n", $lastupdate);

    $rss = new DOMDocument();
    $rss->load($rss_source);


    // load new items
    $feed = array();
    foreach ($rss->getElementsByTagName('item') as $node) {
        $pubdate = $node->getElementsByTagName('pubDate')->item(0)->nodeValue ;
        $ts = rsstotime($pubdate);

        if ( $ts > $maxpubdate ) { $maxpubdate = $ts; }
        
        if ($ts > (int)$lastupdate) {
            $categories = '';
            foreach($node->getElementsByTagName('category') as $c) {
                $categories = $categories.' #'.$c->nodeValue;
            }
            $item = array ( 
                'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                'date' => $pubdate,
                'categories' => $categories,
                );
            array_push($feed, $item);
        }
    }
    if ($maxpubdate > $lastupdate) {
        // record update time
        file_put_contents(LASTUPDATE_FILE, $maxpubdate);
    }


    //  publish items
    if (count($feed) > 0) {
        foreach($feed as $x) {
            $title = str_replace(' & ', ' &amp; ', $x['title']);
            $link = $x['link'];
            $description = strip_tags($x['desc']);
            $categories = $x['categories'];
            $date = date('l F d, Y', strtotime($x['date']));

            if (defined('USE_HUITRE') && USE_HUITRE == 1) {
                require_once 'engines/huitre.php';
                $short = shorten_huitre($link);
                if ($short) { $link = $short; }
            }


            $status = $title. ' : '.$description.' '.$link.' '.$categories;
            echo "Publishing ". $title . " - " . $date . "\n";
            publish($status);
            echo "\n";
            sleep(SLEEPTIME); // to avoid beeing considered as flooder
        } 
    } else {
        echo "No new item\r\n";
    }

}

?>
